+++
title = "Thank you for supporting KiCad!"
date = "2021-03-26"
aliases = [ "/thank-you/" ]
headless = true
summary = "Thank you for your donation"
+++

You are a vital part of the KiCad community.

### https://kicon.kicad.org[[.underline]#Join us this year at KiCon, the annual KiCad conference#]

You will meet other KiCad users and developers.  In recognition of your donation, please use the voucher code **V8SUPPORTER** to receive 20% off your ticket price.  We hope to see you there!

#### KiCad is user-driven and user-supported. Thanks to you, in the past year, we have been able to do the following:

{{< aboutlink "/img/donations/2022_flyer.png" "/img/donations/2022_flyer.png" >}}